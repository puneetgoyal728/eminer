<?php
include_once 'config.php';
include_once 'db_conn.php';
include_once 'paymentHelper.php';
include_once 'common_func.php';

function notifySquadrun($missionId){
    global $squadrun_api_key, $squadrun_api_path;
    $random_salt = createRandom(15);
    //     $random_salt = "kdhakjshd23rdef";
    $padded_api_key = $missionId.$squadrun_api_key.$missionId;
    //     echo "padded_key:".$padded_api_key."<br />";
    $user_name = hash_hmac('sha256', $padded_api_key, $random_salt);
    $options = array(CURLOPT_HTTPHEADER=>array("Secret:$random_salt",
            "Authorization:ApiKey puneet:".$user_name,
            "Content-Type:application/json"
    ),
            CURLOPT_BINARYTRANSFER=> TRUE);
    $post = array("mission_id"=> $missionId);
    $response = curl_post($squadrun_api_path, $post, $options);
    //     print_r($response);

}

function populateTSPDforJobPage ($jid,$pgNo){
    global $conn;
    $jobArr = array();
    // 	$minimumPassVal = getMinVerifyForJob($jid);
    // 	if($minimumPassVal!==0){
    $fetchTsmQuery = "select tsm_id from tbl_snippets_master where job_id =$jid and page_no =$pgNo and processed=0";
    foreach ($conn->dbh->query($fetchTsmQuery) as $row){
        $didgen = assignStateToTSM($row['tsm_id']);
        if(!isset($jobArr[$didGen])){
            $jobArr[$didGen] = true;
        }
    }

    // 		if($useCrowd == "SQUAD"){
    // 		    foreach ($jobArr as $key=>$val){
    // 		        notifySquadrun($key);
    // 		    }
    // 		}
    // 	}
}

// if(isset($_GET['jid']) && isset($_GET['pgno'])){
// // 	populateTSPDforJobPage($_GET['jid'], $_GET['pgno']);
//     notifySquadrun($_GET['jid']);
// }
?>