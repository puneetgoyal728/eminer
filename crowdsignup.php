<?php 
session_start();
if(isset($_SESSION["tcm_id"]) ){
    header("location:crowdsignin.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link type="text/css" rel="stylesheet" href="css/default.css"/>
<link type="text/css" rel="stylesheet" href="css/googlefonts.css"/>
<title>11 TechSquare</title>
<style>
#login label{
    display: inline-block;
    width: 100px;
    text-align: right;
}
#login_submit{
    padding-left: 100px;
}
#login div{
    margin-top: 1em;
}

span .error{
    display: none;
    margin-left: 10px;
}      
 
.error_show{
    color: red;
    margin-left: 10px;
}
input.invalid, textarea.invalid{
    border: 2px solid red;
}
 
input.valid, textarea.valid{
    border: 2px solid green;
}

#loading{
    display: none;
   
}  
</style>
</head>
<body>
    <div id="background">
    <div id="header2">
        <div class="center">
            <a href="index.php">
                <img src="images/logo.png" alt="11 Tech Square"/>
            </a>                        
        </div><!---end of header2 center--->
        
        <div class="center">
            <div id="form2">
                <div class="left">
                    
                </div>
                
                <div class="right">
                <form id="login" action="crowdsignupprocess.php" method="post" onsubmit="document.getElementById('signup').style.display='none'; document.getElementById('loading').style.display='block';">
                    <ul>
                       
                        <li>
                            <input type="text" placeholder="Email" class="inp" id="login_email" name="emailid"/>
                        </li>
                            
                        <li>
                            <input type="password" placeholder="Password" class="inp" id="login_password" name="password"/>
                        </li>
                        <li>
                            <button type="submit" value="Signup" class="submit" id="signup">Signup</button>
                            <img src="images/loading.gif" id="loading" height="42" width="420">
                        </li>
                    </ul>
                    </form>
                </div>
                <div class="clear"></div>
            </div><!---end of form2--->
        </div><!---end of header2 center--->
    </div><!---end of header2--->
    </div><!---end of background--->
    
    <div id="testimonial">
        <div class="center">
            <div class="left">
                <img src="images/P4190729.JPG" alt=""/>
            </div>
            
            <div class="right">
                <span class="spn1">Sameer Jawaharani</span>
                <span class="spn2">Valezco Technology</span>
                <p>
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tempor, mauris vitae lobortis commodo, eros enim congue elit, ut consectetur nibh eros at augue."
                </p>
            </div>
            <div class="clear"></div>
        </div><!---end of testimonial center--->
    </div><!---end of testimonial--->
</body>
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var flag_email=0;
var flag_pass=0;


$('#login_password').on('input', function() {
    var input=$(this);
    var is_name=input.val();
    if(flag_email==0 || flag_pass==0)
    {
        $('.submit').attr("disabled", true);
        $('.submit').css("opacity", 0.30);
    }
    else{
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        }
    if(is_name.length>=6){
        flag_pass=1;
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        input.removeClass("invalid").addClass("valid");
        }
    else{
        flag_pass=0;
        input.removeClass("valid").addClass("invalid");}
});

$('#login_email').on('input', function() {

	if(flag_email==0 || flag_pass==0)
    {
        $('.submit').attr("disabled", true);
        $('.submit').css("opacity", 0.30);
    }
	else{
		$('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
		}
    var input=$(this);
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var is_email=re.test(input.val());
    var emailid=$('#login_email').val();
    var type="crowd";
    if(is_email)
    {
        
    $.ajax({
        type: "POST",
        url: "validateemail.php",
        data: { 'email': emailid,'type' : type}
      }).done(function(data) {
            
            if(data=="0")
            {
                flag_email=1;
            input.removeClass("invalid").addClass("valid");
            }
            else
                {
            	flag_email=0;
                input.removeClass("valid").addClass("invalid");
                }
            });
    
    }
else{
	flag_email=0;
	input.removeClass("valid").addClass("invalid");}
});


</script>
</html>
