<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('common_func.php');
if(!isset($_SESSION["tcm_id"]) || !isset($_GET['jid'])){
    header("location:index.php");
}
$flag=0;
$tcm_id =$_SESSION["tcm_id"];
$tcj_id = $_GET['jid'];
$sendback = getNewTask($tcj_id,$tcm_id);
// print_r($sendback);exit;
if($sendback["status"] == false){
    echo "Sorry we have run out of this catagory of jobs, 
    Please go <a href=\"eminer_home.php\">back</a> and select another catagory.";
    $flag=1;
    exit();
}
?>
<html>
<head>
<title>Enter value as it appear</title>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" ></link>
<link rel="stylesheet" type="text/css" href="css/valueInpPage.css" ></link>
<script src="js/jquery-1.11.1.min.js" ></script>
<script src="js/jquery-ui.js" ></script>
<script src="js/jsrender.min.js" ></script>
</head>
<body>
    <div class="menupane">
            <a href="eminer_home.php">menu</a>
    </div>
    <input type="hidden" id="secure"
        value="<?php echo $sendback["hash"];?>" />
    <input type="hidden" id="skey" value="<?php echo $sendback["salt"];?>" />
    <input type="hidden" id="tcmid" value="<?php echo $tcm_id;?>" />
    <div id="mainpane" class="displayPane">
    </div>
</body>

<?php if($flag==1) echo '<script>$("#mainpane").hide()</script>';?>
<script>
//var availableTags = [ "ActionScript","Harsh", "Hursh", "Hafsh", "Horsh", "Harrh", "AppleScript", "Asp", "BASIC", "C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP", "Python", "Ruby", "Scala", "Scheme" ];

var availableTags = <?php echo json_encode($sendback['auto']);?>;
var tcj_id = <?php echo $_GET['jid'];?>;
var minu = 9;
var sec = 59;

function transition() {

    if(sec>0){
        --sec;
    }else if(sec==0){
        sec=59;
        --minu;
    }
    $('#min').text(minu);
    $('#sec').text(sec);
    if(minu==0 && sec==0)
    	window.location.href='eminer_home.php';
}
setInterval(transition, 1000);

function clockreset(){
    min=9;
    sec=59;
}

function submitdata()
{
    var sendData = new Array();
    $('input.keepingIDs').each(function( index ) {
        var tspdid = $( this ).val();
        var extended = $("input[name=inputOpt"+tspdid+"]:checked").val();
        var valInp = extended;
        var imgSrc = $('#img_'+tspdid).attr('src');
        if(extended == undefined){
            if($("#c_az"+tspdid).length!=0){
                valInp = $("#c_az"+tspdid).val();
            }else{
                var checkedValue = [];
                $('#show'+tspdid+' input:checked').each(function(index){
                    checkedValue.push(this.value);
                });
                valInp = checkedValue.join(',');
            }
        }
        sendData.push({"tspdid":tspdid,"value":valInp,"name":imgSrc});
        //console.log( index + ": " + $( this ).text() );
    });
    $('#mainpane').html("<img src='images/loader.gif'/>");
    var envelop = {'jid':tcj_id,'tcmid':$('#tcmid').val(),'salt':$('#skey').val(),'hash':$('#secure').val(), 'tasks':JSON.stringify(sendData)};
    $.ajax({
        type: "POST",
        url: "testpageresult.php",
        dataType: "json",
        data: envelop
    }).done(function(data) {
        if(data.status==true)
        {
            $('#skey').val(data.salt);
            $('#secure').val(data.hash);
            var template = $.templates("#theTmpl");
            var htmlOutput = template.render(data);
            $("#mainpane").html(htmlOutput);
            defineAction();
            availableTags = data.auto;
            clockreset();
        }
        else
        {
            window.location.href='index.php';
        }
    });
}


function produceList(inp, new_list) {
    var sudoList = [];
    for (var i = new_list.length - 1; i >= 0; i--) {
        if (levDist(inp, new_list[i].substring(0,inp.length)) <2) {
            sudoList.push(new_list[i]);
        }
    };
    return sudoList;
}

//http://www.merriampark.com/ld.htm, http://www.mgilleland.com/ld/ldjavascript.htm, Damerau–Levenshtein distance (Wikipedia)
var levDist = function(s1, s2) {
     var d = []; //2d matrix
     s = s1.toLowerCase();
     t = s2.toLowerCase();
     // Step 1
     var n = s.length;
     var m = t.length;
     
     if (n == 0) return m;
     if (m == 0) return n;
     
     //Create an array of arrays in javascript (a descending loop is quicker)
     for (var i = n; i >= 0; i--) d[i] = [];
     
     // Step 2
     for (var i = n; i >= 0; i--) d[i][0] = i;
     for (var j = m; j >= 0; j--) d[0][j] = j;
     
     // Step 3
     for (var i = 1; i <= n; i++) {
         var s_i = s.charAt(i - 1);
     
         // Step 4
         for (var j = 1; j <= m; j++) {
     
             //Check the jagged ld total so far
             if (i == j && d[i][j] > 4) return n;
     
             var t_j = t.charAt(j - 1);
             var cost = (s_i == t_j) ? 0 : 1; // Step 5
     
             //Calculate the minimum
             var mi = d[i - 1][j] + 1;
             var b = d[i][j - 1] + 1;
             var c = d[i - 1][j - 1] + cost;
     
             if (b < mi) mi = b;
             if (c < mi) mi = c;
     
             d[i][j] = mi; // Step 6
     
             //Damerau transposition
             if (i > 1 && j > 1 && s_i == t.charAt(j - 2) && s.charAt(i - 2) == t_j) {
                 d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + cost);
             }
         }
     }
     
     // Step 7
     return d[n][m];
}

function defineAction(){
    $(".availbuttonscls label input").click(function(e){
        e.stopPropagation();
        var tspdid = $(this).attr("rel");
        if($(this).prop('previousValue') == true){
            $(this).prop("checked",false);
            $("#show"+tspdid).find(':input').prop('disabled',false);
        }else{
            $('input[name=' + $(this).attr('name') + ']').prop('previousValue', false);
            $("#show"+tspdid).find(':input').prop('disabled',true);
        }
        $(this).prop('previousValue', $(this).prop('checked'));
    });
 // var inp = document.getElementById("tags").value;
    $('.suggestbox').on('input', function() { 
        if(availableTags && availableTags.length>1){
            var inp = $(this).val(); // get the current value of the input field.
            if (inp.length >=2) {
                var newList = produceList(inp, availableTags);
                $("#"+this['id']).autocomplete({
                    source: function (request, response) {
                    response(newList);
                    }
                });
            }
        }
    });

    $("#submitResponses").click(function(){
        submitdata();
    });
}
</script>

<script id="theTmpl" type="text/x-jsrender">
        <div id="question_desc" class="head_text">
        <div class="your-clock">Time left to submit data <span id="min"></span>:<span id = "sec"></span></div>
        </div>
        {{for tasks}}
        <div class="eachtask">
            <div class="eachTaskDesc">{{:~root.desc}}</div>
            <img src="{{>serveImg}}" id="img_{{>tspdid}}">
            <div class="optionsdiv">
            {{if ~root.type == 3 && isWithoptions}}
                <div class="checkcls" id="show{{>tspdid}}">
                {{props options}}
                    <div class ="chkbox"><input type='checkbox' name="checks{{>#parent.parent.data.tspdid}}" value="{{>prop}}">{{>prop}}</div>
                {{/props}}
                </div>
            {{/if}}
            {{if ~root.type == 2 && isWithoptions}}
                <div class="radiocls" id="show{{>tspdid}}">
                    {{props options}}
                        <div class ="chkbox"><input name="radiobuttons{{>#parent.parent.data.tspdid}}" type="radio" value="{{>prop}}">{{>prop}}</div>
                    {{/props}}

                </div>
            {{/if}}
            {{if !isWithoptions}}
                <div class="valinpbox" id="show{{>tspdid}}">
                    <input class="suggestbox" placeholder="Enter the value" id="c_az{{>tspdid}}"
                        style="height: 40px; width: 300px; font-size: 20px;" rel="{{>tspdid}}"/>
                </div>
            {{/if}}
                <div class="availbuttonscls">
                    <input type="hidden" value="{{>tspdid}}" class="keepingIDs" />
                    <label class="yellow"><input type="radio" rel="{{>tspdid}}" value="__cantread__" name="inputOpt{{>tspdid}}" /><span>Impossible</span></label>
                    <label class="yellow"><input type="radio" rel="{{>tspdid}}" value="__nothing__" name="inputOpt{{>tspdid}}" /><span>Empty</span></label>
                </div>

                </div>
                <div class="clear"></div>
            </div>
        </div>
        {{/for}}
        <div class="submitpane"><input type="submit" name="submit" id="submitResponses" value="Submit" class="sbmtbtn" /> </div>
</script>
<script type="text/javascript">
var template = $.templates("#theTmpl");

var htmlOutput = template.render(<?php echo json_encode($sendback); ?>);

$("#mainpane").html(htmlOutput);
defineAction();
</script>
</html>
