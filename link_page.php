
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="css/cr_login.css">
<!-- Bootstrap Core CSS -->
   
<style>

.header{
	position: absolute;
	top: calc(30% - 30px);
	left: calc(3% - 10px);
	z-index: 2;
}

.header div{
	float: left;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 35px;
	font-weight: 500;
}

.header div span{
	color: #5379fa !important;
}
</style>

</head>
<body>
	<div class="header">
		<div>
			Activation link has been sent to your mailbox.<span><p>Activate your account and <a href="">log in.</a><p></span>
		</div>
	</div>
</body>

</html>
