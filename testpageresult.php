<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('common_func.php');
include_once 'paymentHelper.php';
$salt= trim($_POST['salt']);
$text= trim($_POST['hash']);
$tcj_id = trim($_POST['jid']);
$curtcmid = trim($_POST['tcmid']);
$tasksArr = json_decode(trim($_POST['tasks']),true);

$sendback = array();

$maxAllowedStages = 2;

$gotkey=decode($salt,$text);

if($gotkey==$curtcmid."||".$tcj_id)
{
    foreach ($tasksArr as $task){
        $n= trim($task['name']);
        $value= htmlspecialchars(trim($task['value']),ENT_QUOTES);
        if($value == ''){
            
            continue;
        }
        $tspd_id = trim($task['tspdid']);
//         unlink($n);

        $updateTSPDQry = $conn->dbh->prepare("update tbl_snippet_process_data set
                data_entered = '".$value."', tspd_status = 2, submit_date=CURDATE(), submit_time=CURTIME()
                where tspd_id = '".$tspd_id."' and tcm_id='".$curtcmid."' and tspd_status=1 and tcj_id=$tcj_id");
        $updateTSPDQry->execute();
        $updateCount = $updateTSPDQry->rowCount();
        if($updateCount!=1){
            $sendback['status'] = false;
            echo json_encode($sendback);
            exit;
        }
        $getTsmQuery = "select tspd.tsm_id tsm_id, tsm.job_id job_id,tsm.stateType stateType,
        tsm.stateStage stateStage, tsm.currCycleNo, tspd.data_entered data_entered
        from tbl_snippet_process_data tspd join tbl_snippets_master tsm
        on tspd.tsm_id=tsm.tsm_id where tspd.tspd_id=".$tspd_id;
        $getTsmRes = $conn->dbh->query($getTsmQuery);
        $getTsmResult = $getTsmRes->fetch(PDO::FETCH_ASSOC);
        if($getTsmResult){
//             $minimumPassVal = getMinVerifyForJob($getTsmResult[0]['job_id']);
//             if(strcmp($minimumPassVal,1)==0){
//                 setCorrectValue($getTsmResult[0]['tsm_id'],$getTsmResult[0]['data_entered']);
//                 grantPayment($curtcmid, $tspd_id, $tcj_id);
//             }else{
            $dataEntered = getAllResponsesForTSM($getTsmResult['tsm_id']);
            if($dataEntered['status']==1){
                $evaluatedData = evaluateResponses($dataEntered['data'],$minimumPassVal);
                if($evaluatedData['correctVal']){
//                  print_r($evaluatedData);
                    setCorrectValue($getTsmResult['tsm_id'],$evaluatedData['correctVal']);
                    foreach ($evaluatedData['correct'] as $correctResponses){
                        grantPayment($correctResponses[1], $correctResponses[0], $tcj_id);
                    }
                    foreach ($evaluatedData['wrong'] as $wrongResponses){
                        denyPayment($wrongResponses[1], $wrongResponses[0], $tcj_id);
                    }
                }else if(count($dataEntered['data'])<$maxAllowedDuplicates){
                    generateTSPDEntriesForTSM($getTsmResult['tsm_id'], 1);
                }
            }
//             }
        }
    }
    if(isset($_SESSION["tcm_id"]))
    {
        $sendback = getNewTask($tcj_id,$_SESSION["tcm_id"]);
    }
    else {
        $sendback['status'] = false;
    }
}
else {
    $sendback['status'] = false;
}
echo json_encode($sendback);
// echo $gotkey."==".$tcm_id."||".$n."||".$tcj_id."||".$tspd_id;
    ?>