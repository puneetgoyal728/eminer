<?php
session_start();
if(!isset($_SESSION["tcm_id"]))
{
    header("location:index.php");
    exit();
}
include_once('config.php');
include_once ('db_conn.php');
?>
<!DOCTYPE html>
<html lang="en">
<style>
input{
display: block;
  width: 40%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
#loading {
width:40%;
display:none;
}
#signup {
width:40%;
}
.invalid{
    color:  red;
}
 
.valid{
    color:  green;
}
</style>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- DataTables CSS -->
    <link href="css/plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
              <?php include_once 'topnav.php';?>
           <?php include_once 'sidenav.php';?>
        </nav>
    </div>
    <!-- Wrapper -->
    <div id="page-wrapper" style="min-height: 378px;">
        <div class="row">
    <div class="panel-body">
                        <div>
                             <fieldset>
                             
                             <div class="form-group" > <span id="note"></span></div>
                                <div class="form-group">
                                    <input placeholder="old password" name="old" id="old" type="password" >
                                </div>
                                <div class="form-group">
                                    <input  placeholder="new password" name="new" id="new" type="password" >
                                </div>
                                <div class="form-group">
                                    <input placeholder="confirm new password" name="confirmnew" id="confirmnew" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" value="Signup" class="submit btn btn-success" id="signup">Change</button>
                            <img src="images/loading.gif" id="loading" height="42" width="420">
                            </fieldset>
                        </div>
                    </div>
    </div></div>
    <!-- jQuery -->
    

    <script src="js/bootstrap.min.js"></script>

  
    
        </body>
<script>

$('#new').on('input', function() {
    var newpass=$('#new').val();
    var confirmpass=$('#confirmnew').val();
    if(newpass.length>=6 && newpass==confirmpass){
       
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        $('#new').removeClass("invalid").addClass("valid");
        $('#confirmnew').removeClass("invalid").addClass("valid");
        }
    else{
    	$('#new').removeClass("valid").addClass("invalid");
    	$('#confirmnew').removeClass("valid").addClass("invalid");}
 
});
$('#confirmnew').on('input', function() {
    var newpass=$('#new').val();
    var confirmpass=$('#confirmnew').val();
    if(newpass.length>=6 && newpass==confirmpass){
       
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        $('#new').removeClass("invalid").addClass("valid");
        $('#confirmnew').removeClass("invalid").addClass("valid");
        }
    else{
        $('#new').removeClass("valid").addClass("invalid");
        $('#confirmnew').removeClass("valid").addClass("invalid");}
 
});
$("#signup").click(function() {
    var oldpass=$('#old').val();
    var curpass=$('#new').val();
    $.ajax({
     type: "POST",
      url: "changepasswordprocess.php",
      data: { 'oldpass': oldpass,'newpass':curpass }
    }).done(function(data) {
    if(data==1)
    {
        $('#note').text("Password has been changed");
        $('#note').removeClass("invalid").addClass("valid");
      
    }
        
    else
    {
    	$('#note').text("Incorrect old password");
        $('#note').removeClass("valid").addClass("invalid");
    }
    });
});
</script>
</html>
