<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('common_func.php');
include_once 'paymentHelper.php';
$salt= trim($_POST['salt']);
$text= trim($_POST['hash']);
$curtcmid = trim($_POST['tcmid']);
$tasksArr = json_decode(trim($_POST['tasks']),true);
// print_r($tasksArr);exit;
$gotkey=decode($salt,$text);


function entryVotingLog($tsm_id,$tcm_id,$value,$voted_tspd,$voted_tcm){
    global $conn;
    $votingValQry = "insert into tbl_temp_voting_log (tsm_id, tcm_id, value_voted, voted_tspd_id, voted_tcm_id, RecAddDate, RecAddTime) 
    values ('".$tsm_id."','".$tcm_id."','".$value."','".$voted_tspd."','".$voted_tcm."',CURDATE(),CURTIME())";
    $queryRes = $conn->dbh->query($votingValQry);
}

foreach ($tasksArr as $value) {
    $votedForQuery = "select tcm_id from tbl_snippet_process_data where tsm_id = '".$value['tsmid']."' and data_entered = '".$value['value']."' and tspd_id = '".$value['tspdid']."'";
    $votedForRes = $conn->dbh->query($votedForQuery);
    $votedResult = $votedForRes->fetch(PDO::FETCH_ASSOC);
    if($votedResult){
        setCorrectValue($value['tsmid'],$value['value']);
        grantPayment($votedResult['tcm_id'], $value['tspdid'], 1);
        entryVotingLog($value['tsmid'],$votedResult['tcm_id'],$value['value'],$value['tspdid'],$votedResult['tcm_id']);
    }
}
echo json_encode(array('status'=>true));