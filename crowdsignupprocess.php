<?php
include_once('config.php');
include_once 'db_conn.php';
include 'scrypt.php';
include_once('common_func.php');
$name=$_POST['name'];
$emailid= $_POST['email'];
$password=$_POST['password'];
$hashpwd = Password::hash($password);
$salt=createRandom(10);
$today = date("Y-m-d"); 
$text=$password."||".$salt."||".$today;
$activationlink=encode($salt,$text);

$userCreateQry = "INSERT INTO tbl_crowd_master(useremail,name,password,userlevel,activationlink,recadddate,recaddtime) VALUES (:emailid,:name,:hashpwd,0,:activationlink,CURDATE(),CURTIME())";
$sth = $conn->dbh->prepare($userCreateQry);
$sth->execute(array(':emailid'=>$emailid,':name'=>$name,':hashpwd'=>$hashpwd,':activationlink'=>$activationlink)) or die(var_dump($sth->errorInfo()));

$message= $basePath."validate.php?email=".$emailid."&t=".$salt."&s=".urlencode($activationlink); 
$subject = "Verify your email";
$body = "Dear ".$emailid."<br /><br />Thank you for signing up with us. Your username is ".$emailid."
Please proceed to<a href='".$message."'>".$message."</a> to activate your account. <br /><br />
Yours Sincerely<br /><br />
11TechSquare Team";
//sendmail($emailid,0,$body,$subject);
//if($sendmail) {
//    echo "The activation link has been sent to your email. Please click on it to complete your Registration process. <br />
//    Don't forget to check spam mails in case our mail arrve at spam box, add the mailer in your contacts to avoid receiving in spam folder in future.";
//    
//}
?>

<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="css/cr_login.css">
<!-- Bootstrap Core CSS -->
   
<style>

.header{
    position: absolute;
    top: calc(30% - 30px);
    left: calc(3% - 10px);
    z-index: 2;
}

.header div{
    float: left;
    color: #fff;
    font-family: 'Exo', sans-serif;
    font-size: 35px;
    font-weight: 500;
}

.header div span{
    color: #5379fa !important;
}
</style>

</head>
<body>
    <div class="header">
        <div>
            Activation link has been sent to <?php echo $emailid;?>.<span><p>Activate your account and <a href="index.php">log in.</a><p></span>
        </div>
        <?php echo $body;?>
    </div>
</body>

</html>
