function multiUploader(config){
  
	this.config = config;
	this.items = "";
	this.all = []
	var self = this;
	
	multiUploader.prototype._init = function(){
		if (window.File && 
			window.FileReader && 
			window.FileList && 
			window.Blob) {		
			 var inputId = $("#"+this.config.form).find("input[type='file']").eq(0).attr("id");
			 document.getElementById(inputId).addEventListener("change", this._read, false);
			 document.getElementById(this.config.dragArea).addEventListener("dragover", function(e){ e.stopPropagation(); e.preventDefault(); }, false);
			 document.getElementById(this.config.dragArea).addEventListener("drop", this._dropFiles, false);
			 //document.getElementById(this.config.form).addEventListener("submit", this._submit, false);
			 $('#'+this.config.form).submit(function(){
	        		 multiUploader.prototype._submit();
        			 return false;
			 });
		} else
			console.log("Browser supports failed");
	}
	
	multiUploader.prototype._submit = function(e){
		//e.stopPropagation(); e.preventDefault();
		self._startUpload();
	}
	
	multiUploader.prototype._preview = function(data){
		this.items = data;
		if(this.items.length > 0){
			var html = "";		
			var uId = "";
 			for(var i = 0; i<this.items.length; i++){
				uId = this.items[i].name._unique();
				var sampleIcon = '<img src="images/image.png" />';
				var errorClass = "";
				if(typeof this.items[i] != undefined){
					if(self._validate(this.items[i].type) <= 0) {
						sampleIcon = '<img src="images/unknown.png" />';
						errorClass =" invalid";
					} 
					html += '<div class="dfiles'+errorClass+'" rel="'+uId+'"><h5>'+sampleIcon+this.items[i].name+'</h5><div id="'+uId+'" class="progress" style="display:none;"><img src="images/ajax-loader.gif" /></div></div>';
				}
			}
			$("#dragAndDropFiles").append(html);
		}
	}

	multiUploader.prototype._read = function(evt){
		if(evt.target.files){
			self._preview(evt.target.files);
			self.all.push(evt.target.files);
			self._startUpload();
		} else 
			console.log("Failed file reading");
	}
	
	multiUploader.prototype._validate = function(format){
		var arr = this.config.support.split(",");
		return arr.indexOf(format);
	}
	
	multiUploader.prototype._dropFiles = function(e){
		e.stopPropagation(); e.preventDefault();
		self._preview(e.dataTransfer.files);
		self.all.push(e.dataTransfer.files);
		self._startUpload();
	}
	
	multiUploader.prototype._uploader = function(file,f){
	        disableFormSubmit();
		if(typeof file[f] != undefined && self._validate(file[f].type) > 0){
			var data = new FormData();
			var ids = file[f].name._unique();
			data.append('file',file[f]);
			data.append('index',ids);
			$(".dfiles[rel='"+ids+"']").find(".progress").show();
			$.ajax({
				type:"POST",
				url:this.config.uploadUrl,
				data:data,
				cache: false,
				contentType: false,
				processData: false,
				success:function(rponse){
				        response = rponse.split("|");
					$("#"+ids).hide();
					var obj = $(".dfiles").get();
					$.each(obj,function(k,fle){
						if($(fle).attr("rel") == response[0]){
							$(fle).slideUp("normal", function(){ $(this).remove(); });
							var newVal = (($("#image_list").val()=='')?"":($("#image_list").val()+";")) + response[1];
							$("#image_list").val(newVal);
							var addedImgHtml = '<div class="ed_images"><img src="'+(response[1])+'"class="fltL"/><div class="fltL delimg" rel="'+(response[1])+'">x</div><div class="clr"></div></div>';
							$(".ed_images_hold").prepend(addedImgHtml);
						}
					});
					if (f+1 < file.length) {
						self._uploader(file,f+1);
					}else{
					        enableFormSubmit();
					}
				}
			});
		} else
			console.log("Invalid file format - "+file[f].name);
	}
	
	multiUploader.prototype._startUpload = function(){
		if(this.all.length > 0){
			for(var k=0; k<this.all.length; k++){
				var file = this.all[k];
				this._uploader(file,0);
				
			}
		}/*else{
	                $("#addElemForm").submit();
		}*/
		this.all = [];
	}
	
	String.prototype._unique = function(){
		return this.replace(/[a-zA-Z]/g, function(c){
     	   return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
    	});
	}

	this._init();
}

function deleteAddedImg (domelem){
        var imgToDel = $(domelem).attr("rel");
        //alert(imgToDel);
        var allImgs = $('#image_list').val();
        var allImgsArr = allImgs.split(";");
        var toDelInd = $.inArray(imgToDel,allImgsArr);
        allImgsArr.splice(toDelInd,1);
        var newValStr = allImgsArr.join(";");
        $('#image_list').val(newValStr);
        $(domelem).parent().remove();
}
function disableFormSubmit(){
        $("#submit_elem_add").addClass("disable");
        $("#submit_elem_add").attr("disabled","disabled");

}

function enableFormSubmit(){
        $("#submit_elem_add").removeAttr("disabled");
        $("#submit_elem_add").removeClass("disable");
}
function initMultiUploader(){
	new multiUploader(config);

$(".ed_images_hold").delegate(".delimg","click",function(){
        deleteAddedImg (this);
});
}
