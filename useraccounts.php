<?php
session_start();
include_once("config.php");
include_once ('db_conn.php');
if(!isset($_SESSION["tcm_id"])){
    header("location:index.php");
}
$userid = $_SESSION["tcm_id"];

foreach( $conn->dbh->query("SELECT * from tbl_crowd_master where tcm_id=$userid" ) as $row)
{
    $name=$row["name"];
    $withdrawl=$row["lastwithdrawl"];
    $wallet=$row["currentwallet"];
    $emailid=$row["useremail"];

}
$flag=0;
if(isset($_GET['f']))
{
 $flag=$_GET['f'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Home</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">


<!-- DataTables CSS -->
<link href="css/plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<style>
.msg0 {
	opacity: 1;
	width: 350px;
	margin: 4px;
	background-color: #EFEFEF;
	border-radius: 4px;
	border: 1px solid #D0D0D0;
}

.msg1 {
	opacity: 1;
	width: 350px;
	margin: 4px;
	background-color: #EFEFEF;
	border-radius: 4px;
	border: 1px solid #D0D0D0;
}
</style>
<body>

	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<?php include_once 'topnav.php';?>
			<?php include_once 'sidenav.php';?>
		</nav>
	</div>
	<!-- Wrapper -->
	<!-- DIV for the actual page -->
	<div id="page-wrapper" style="min-height: 378px;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">My Profile</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<!-- /.panel-heading -->
					<div class="panel-body">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs">
							<li class="<?php if($flag==0) echo 'active';?>" id="hometab"><a href="#home" data-toggle="tab"
								aria-expanded="<?php if($flag==0) echo 'true';?>">Profile</a>
							</li>
							<li class="<?php if($flag==1) echo 'active';?>" id="messagetab"><a href="#messages" data-toggle="tab"
								aria-expanded="<?php if($flag==1) echo 'true';?>">Messages</a>
							</li>
							<li class=""><a href="#wallet" data-toggle="tab"
								aria-expanded="false">Wallet</a>
							</li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div class="tab-pane fade <?php if($flag==0) echo 'active in';?> in" id="home">
								<br>
								<p>
									Name :
									<?php echo $name?>
									<br>Email :
									<?php echo $emailid?>
									<br>
								</p>
							</div>
							<div class="tab-pane fade <?php if($flag==1) echo 'active in';?>" id="messages">
								<?php foreach ($DataResult as $key => $result){
								    echo '<br><div class="msg'.$result["seen"].'" >'.$result["notification"].'</div>';
                                    }?>
							</div>
							<div class="tab-pane fade" id="wallet">
								<br>
								<p>
									Current Balance :
									<?php echo $wallet?>
									<br>Last withdrawl Amount :
									<?php echo $withdrawl?>
									<br>
								</p>
							</div>

						</div>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-6 -->
		</div>
	</div>
	<!-- jQuery -->
	<script src="js/jquery-1.11.1.min.js"></script>

	<script src="js/bootstrap.min.js"></script>



</body>

</html>
