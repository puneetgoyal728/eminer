<?php
include_once('config.php');
include_once ('db_conn.php');
include_once('common_func.php');
$p=0;
if(isset($_GET['email']))
{
$email= $_GET['email'];
$s= $_GET['s'];
$salt=$_GET['t'];
$today = strtotime(date("Y-m-d"));
$gotkey=decode($salt,$s);
$array=explode("||",$gotkey);
$date1 = DateTime::createFromFormat("Y-m-d",date("Y-m-d"));
$date2 = DateTime::createFromFormat("Y-m-d",$array[2]);
$interval = date_diff($date1,$date2);
$count=0;
    if($interval->d<2)
    {
       
        foreach( $conn->dbh->query("select userlevel,useremail from tbl_crowd_master where useremail='".$email."' and activationlink='".$s."' " ) as $row)
            {
                $count =$row['userlevel'];
            }
        if($count==1)
    
    $p=1;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sign Up</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

</head>
<style>
input{
display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
input.invalid, textarea.invalid{
    border: 2px solid red;
}
 
input.valid, textarea.valid{
    border: 2px solid green;
}
#loading{
    width:300px;
    display: none;
   
}
ul
{
    list-style-type: none;
}
</style>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">change password</h3>
                    </div>
                    <div class="panel-body">
                        <form id="forgotpass" action="setnewpassword.php" method="post">
                            <?php if($p==1){
                            ?> 
                          
                             <ul><li><input type="password" placeholder="New Password" name="newpass" class="inp"/><br>
                             </li><li><input type="password" placeholder="Confirm Password" class="inp"/><br>
                             <input type="hidden" name="hash"  value = "<?php echo $s?>" class="inp"/>
                             <input type="hidden" name="salt"  value = "<?php echo $salt?>" class="inp"/>
                             <input type="submit" value="Change password" class="submit"/></li>
                             </ul>
                             
                            <?php }?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="js/jquery-1.11.1.min.js"></script>
<script>
if(<?php echo $p?>==1)
$("#forgotpass").append('');
</script>

</html>