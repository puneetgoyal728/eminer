<?php
include_once 'config.php';
include_once 'db_conn.php';
define('TimeMinutesThresh', 10);

$currMysqlTimeQry = "Select now() now";
$currMysqlTimeRes = $conn->dbh->query($currMysqlTimeQry);
$currMysqlTimeResult = $currMysqlTimeRes->fetch(PDO::FETCH_ASSOC);

$refTime = DateTime::createFromFormat("Y-m-d H:i:s",$currMysqlTimeResult['now']);

$tspd_data_query = "select tspd_id, Recservedate, Recservetime from tbl_snippet_process_data where tspd_status =1";
foreach ($conn->dbh->query($tspd_data_query) as $tspd_data_result){
    $serveDate = $tspd_data_result['Recservedate'];
    $serveTime = $tspd_data_result['Recservetime'];
    $serveFormatTime = DateTime::createFromFormat("Y-m-d H:i:s",$serveDate." ".$serveTime);

    $interval = $serveFormatTime->diff($refTime);
    if($interval->i > TimeMinutesThresh){
        $tspd_update_query = "update tbl_snippet_process_data set
        tspd_status = 0, Recservedate = null, Recservetime = null, tcm_id = null, serve_name = null
        where tspd_id = ".$tspd_data_result['tspd_id'];
        $tspd_update_res = $conn->dbh->query($tspd_update_query);
    }
}
?>