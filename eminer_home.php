<?php
session_start();
if(!isset($_SESSION["tcm_id"]))
{
    header("location:index.php");
    exit();
}
include_once('config.php');
include_once ('db_conn.php');
$userid=$_SESSION["tcm_id"];
$jobData = $conn->dbh->query('SELECT tcj_id, heading, description, created_by FROM tbl_crowd_jobs');
$jobDataResult = $jobData->fetchAll(PDO::FETCH_ASSOC);
$temp=array();
foreach ($jobDataResult as $key => $result){
 array_push($temp, array("title"=>$result["heading"], "description"=>$result["description"],"link"=>"testpage.php?jid=".$result['tcj_id']));
}
$temp = json_encode($temp);



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- DataTables CSS -->
    <link href="css/plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
              <?php include_once 'topnav.php';?>
           <?php include_once 'sidenav.php';?>
        </nav>
    <div id="page-wrapper" style="min-height: 378px;">
        <div class="row">
    <div class="col-lg-12">
    <br>
        <div class="panel panel-default">
            <div class="panel-heading">
                Choose Your Task
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Description</th>
                                
                            </tr>
                        </thead>
                        <tbody id="index_table">
                           
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    </div></div>
    </div>
    <!-- Wrapper -->
    <!-- jQuery -->
    <script src="js/jquery-1.11.1.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function(){
            
            
            var temp = <?php echo $temp;?>;
            // temp = JSON.parse(temp);
            // alert("hello  "+ temp);
            for (var i = 0; i <temp.length; i++) {
                // temp[i]
                var note = "<tr> <td>"+(i+1)+"</td><td><a href="+temp[i]["link"]+">"+temp[i]["title"]+"</a></td><td>"+temp[i]["description"]+"</td> </tr>";
                $("#index_table").append(note); 
            };
            
        });
    </script>
    
        </body>
</html>
