<?php 
session_start();
include_once ('db_conn.php');
include_once 'scrypt.php';
include_once 'common_func.php';
if(isset($_SESSION["tcm_id"])){
    header("location:eminer_home.php");
}
$errtxt = "";
if(isset($_POST['emailid']) && isset($_POST['password'])){
    $emailid= $_POST['emailid'];
    $password=$_POST['password'];
    $passCheck = 'SELECT tcm_id,password, userlevel FROM tbl_crowd_master where useremail = "'.$emailid.'"';
    $passRes = $conn->dbh->query($passCheck);
    $passResult = $passRes->fetch(PDo::FETCH_ASSOC);
    if($passResult){
        if( Password::check($password,$passResult['password']) && $passResult['userlevel']==1)
        {
            $_SESSION["tcm_id"]=$passResult['tcm_id'];
            header("location:eminer_home.php");
        }
        elseif($passResult['userlevel']!=1)
        {
            $errtxt = "Account not activated";
        }else{
            $errtxt = "Wrong username or password";
        }
    }else{
        $errtxt = "Wrong username or password";
    }
}
?>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="<?php echo auto_version("/css/cr_login.css"); ?>">
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script>
function loaderActivate(){
        $(".submitbtn").hide();
        $("#loading").show();
}
</script>
</head>
<body>
	<div class="header">
		<div>
			My<span>Job</span>
		</div>
	</div>
	<div class="errtxt">
		<div>
			<?php echo $errtxt;?>
		</div>
	</div>
	<div class="login">
		<form id="myLoginForm" action="" method="post"
			onsubmit="loaderActivate();">
			<input type="text" class="textinp" placeholder="emailid"
				name="emailid"><br> <input type="password" class="passinp"
				placeholder="password" name="password"> <br><a style="width:100px;color: #fff;" href="forgotpassword.php">forgot password</a>
                <br><input type="submit" value="Login" class="submitbtn"> <img width="260" height="34"
				style="display: none; margin-top: 10px;" id="loading"
				src="images/cr_bt.gif">
		</form>
            <!-- <div style="position:relative;left: 300px;top:-84px">
            <p>Dont have an account?</p>
            <a href="sign_up.php"><button style="width:260px;" class="submitbtn" >signup</button></a>
            </div> -->
	</div>
</body>
<script type="text/javascript">
$(".errtxt").delay(5000).fadeOut();
</script>
</html>
