<?php
session_start();
include_once('config.php');
include_once 'db_conn.php';
include 'scrypt.php';
include_once('common_func.php');
$flag=0;
$newpass=$_POST['newpass'];
$hash=$_POST['hash'];
$hashpwd = Password::hash($newpass);
$userCreateQry = "update tbl_crowd_master set password=:hashpwd where  activationlink=:hash";
     $sth = $conn->dbh->prepare($userCreateQry);
     $sth->execute(array(':hashpwd'=> $hashpwd,':hash'=> $hash)) or die(var_dump($sth->errorInfo()));
     $conn->dbh->exec($userCreateQry);

?>

<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="css/cr_login.css">
<!-- Bootstrap Core CSS -->
   
<style>

.header{
    position: absolute;
    top: calc(30% - 30px);
    left: calc(3% - 10px);
    z-index: 2;
}

.header div{
    float: left;
    color: #fff;
    font-family: 'Exo', sans-serif;
    font-size: 35px;
    font-weight: 500;
}

.header div span{
    color: #5379fa !important;
}
</style>

</head>
<body>
    <div class="header">
        <div>
            Your password has been changed.<span> <a href="index.php">log in.</a></span>
        </div>
    </div>
</body>

</html>
