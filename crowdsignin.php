<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
if(!isset($_SESSION["tcm_id"]))
{
    header("Location:index.php");
    exit();
}else{
    header('Location: eminer_home.php');
}
$jobData = $conn->dbh->query('SELECT tcj_id, heading, description, created_by FROM tbl_crowd_jobs');
$jobDataResult = $jobData->fetchAll(PDO::FETCH_ASSOC);
?>
<html>
<style>
a {
	width: 400px;
	color: #92AAB0;
	text-align: left;
	vertical-align: middle;
	font-size: 100%;
}
</style>
<script src="js/jquery-1.11.1.min.js"></script>
<body>
	<a href="crowdlogout.php"> logout</a>
	<table id="table">
		<tr>
			<th>sr no.</th>
			<th>title</th>
			<th>Description</th>
		</tr>
		<?php 
		foreach ($jobDataResult as $key => $result){
		    echo "<tr><td><a class= 'button' href=testpage.php?jid=".$result['tcj_id']."><p>".$key."</p></a></td>
		    <td> <a class= 'button' href='testpage.php?jid=".$result['tcj_id']."' title='".$result['description']."'><p>".$result['heading']."</p></a></td>
		    <td><p>".$result['created_by']."</p></td></tr>";
		}
		?>
	</table>
</body>
</html>