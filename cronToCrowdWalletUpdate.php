<?php

include_once 'db_conn.php';
// set the PDO error mode to exception
$conn->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$tcwlQuery = "select tcm_id, sum(amount) amount from tbl_crowd_wallet_log where tcwl_status=0 and RecAddDate < date(date_sub(CURDATE(), interval 1 day)) group by tcm_id;";

$result = $conn->dbh->query($tcwlQuery);
$tcwlOP = $result->fetchAll(PDO::FETCH_ASSOC);
foreach ($tcwlOP as $row){
    update_account_log($row['tcm_id'], $row['amount']);
}

function update_account_log($tcm_id, $amount)
{
	global $conn;
	try {
 		$conn->dbh->beginTransaction();
		$tcalInsert = "Insert into tbl_crowd_account_log(tcm_id, amount, type, rec_add_date, rec_add_time, trans_details) values( ".$tcm_id.", ".$amount.", 'Credit', CURDATE(), CURTIME(), 'Added amount at the end of day');";
		$result = $conn->dbh->exec($tcalInsert);

		$tcmUpdate = "Update tbl_crowd_master set currentwallet = currentwallet + ".$amount." where tcm_id=".$tcm_id.";";
		$result = $conn->dbh->exec($tcmUpdate);	

		$tcnInsert = "Insert into tbl_crowd_notification(tcm_id, notification, rec_add_date, rec_add_time) values( ".$tcm_id.", 'Added Rs. ".$amount." to wallet', CURDATE(), CURTIME());";
		$result = $conn->dbh->exec($tcnInsert);	

		$tcwlUpdate = "Update tbl_crowd_wallet_log set tcwl_status=1 where tcwl_status=0 and RecAddDate < date(date_sub(CURDATE(), interval 1 day)) and tcm_id =".$tcm_id.";";
		$result = $conn->dbh->exec($tcwlUpdate);

		$conn->dbh->commit();
 		return TRUE;
 
	} catch (Exception $e) {
		$conn->dbh->rollBack();
 		return FALSE;
	}
}

?>

