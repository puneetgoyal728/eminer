<?php
session_start();
include_once('config.php');
include_once ('db_conn.php');
include_once('common_func.php');
if(!isset($_SESSION["tcm_id"])){
    header("location:index.php");
}
// if($_GET['jid'] != 4){
//     header("location:crowdlogin.php");
// }

function getVotingTask($tcm_id){
    global $conn,$displayToCrowd;
    $notInclude = csvListTSMUser($tcm_id);
    $returnValArr = array();
    
    $unConvergedQry = "select tsm.tsm_id tsm_id,tsm.job_id job_id, tsm.field_id field_id, 
    tjd.form_page_no form_page_no, tjd.form_id form_id, ttm.instructions descript
    from tbl_snippets_master tsm join tbl_snippet_process_data tspd 
    join tbl_job_details tjd join tbl_template_master ttm 
    on tsm.tsm_id = tspd.tsm_id and tsm.job_id = tjd.job_id 
    and tsm.page_no=tjd.page_no and ttm.page_no=tjd.form_page_no 
    and ttm.form_id=tjd.form_id and ttm.field_id=tsm.field_id 
    where tsm.processed = 0 and tsm.tsm_id not in (".$notInclude.") 
    group by tsm.tsm_id 
    having count(tspd.tspd_id) = sum(tspd.tspd_status = 2) 
    order by tsm.field_id, tjd.form_page_no, tjd.form_id limit $displayToCrowd";
    
//     echo $unConvergedQry;exit;
    $unConvergedRes = $conn->dbh->query($unConvergedQry);
    $jobResult = $unConvergedRes->fetchAll(PDO::FETCH_ASSOC);
//     print_r($jobResult);exit;
    if(count($jobResult) >= 1)
    {
        $returnValArr['status'] = true;
        $returnValArr['tasks'] = array();
        foreach ($jobResult as $job){
            $eachSolutionQry = "select tspd_id, data_entered from tbl_snippet_process_data where tsm_id = '".$job['tsm_id']."'";
            $eachSolRes = $conn->dbh->query($eachSolutionQry);
            $eachSolResult = $eachSolRes->fetchAll(PDO::FETCH_ASSOC);
            $thisJobArray = array();
            foreach ($eachSolResult as $eachSol) {
                array_push($thisJobArray,$eachSol);
            }
            $task = array();
            $task['serveImg'] = "snippets/".$job['job_id']."/".$job['tsm_id'].".jpg";
            $task['desc'] = $job['descript'];
            $task['responses'] = $thisJobArray;
            $task['tsm_id'] = $job['tsm_id'];
            array_push($returnValArr['tasks'],$task);
        }
    }else{
        $returnValArr['status'] = false;
        
    }
    return $returnValArr;
}

$flag=0;
$tcm_id =$_SESSION["tcm_id"];
$sendback = getVotingTask($tcm_id);
// print_r($sendback);exit;
if($sendback["status"] == false){
    echo "Sorry we have run out of this catagory of jobs, Please go <a href=\"testpage.php\">back</a> and select another catagory.";
    $flag=1;
}
?>
<html>
<head>
<title>Enter value as it appear</title>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css"></link>
<style>
img {
    max-width: 100%;
}

.head_text {
    background-color: #fff;
    color: #e11111;
    font-size: 20px;
    font-weight: 600;
    padding: 10px 0;
    position: fixed;
    top: 25px;
    width: 100%;
}

.valinpbox {
    float: left;
}

.availbuttonscls {
    float: left;
}

.clear {
    clear: both;
}

.eachtask{
    margin: 25px auto;
    padding: 5px;
    border: 1px solid;
    width: 1000px;
}

.optionsdiv{
    margin: 5px auto;
    width: 80%;
}

.availbuttonscls label {
    float:left;
    width:170px;
    margin:4px;
    background-color:#EFEFEF;
    border-radius:4px;
    border:1px solid #D0D0D0;
    overflow:auto;
    cursor: pointer;
}

.availbuttonscls label span {
    text-align:center;
    font-size: 15px;
    padding:5px 0px;
    display:block;
}

.availbuttonscls label input {
    position:absolute;
    top:-20px;
    visibility: hidden;
}

.availbuttonscls input:checked + span {
    background-color:#404040;
    color:#F7F7F7;
}

.availbuttonscls .yellow {
    background-color:#FFCC00;
    color:#333;
}

.sbmtbtn {
   border-top: 1px solid #b9bf8f;
   background: #4a4937;
   background: -webkit-gradient(linear, left top, left bottom, from(#ffffe8), to(#4a4937));
   background: -webkit-linear-gradient(top, #ffffe8, #4a4937);
   background: -moz-linear-gradient(top, #ffffe8, #4a4937);
   background: -ms-linear-gradient(top, #ffffe8, #4a4937);
   background: -o-linear-gradient(top, #ffffe8, #4a4937);
   padding: 9.5px 19px;
   -webkit-border-radius: 5px;
   -moz-border-radius: 5px;
   border-radius: 5px;
   -webkit-box-shadow: rgba(0,0,0,1) 0 1px 0;
   -moz-box-shadow: rgba(0,0,0,1) 0 1px 0;
   box-shadow: rgba(0,0,0,1) 0 1px 0;
   text-shadow: rgba(0,0,0,.4) 0 1px 0;
   color: white;
   font-size: 19px;
   font-family: Georgia, serif;
   text-decoration: none;
   vertical-align: middle;
   width: 150px;
   }
   
.sbmtbtn:hover {
   border-top-color: #75726a;
   background: #75726a;
   color: #ccc;
   }
   
.sbmtbtn:active {
   border-top-color: #4e4f46;
   background: #4e4f46;
   }
   
.displayPane {
    margin-top: 100px;
}

.menupane {
    background-color: #fff;
    font-size: 18px;
    font-weight: 600;
    height: 30px;
    position: fixed;
    top: 0;
    width: 100%;
}

.eachTaskDesc {
    font-weight: 600;
    margin: 5px 0;

}
</style>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jsrender.min.js"></script>
</head>
<body>
    <div class="menupane">
            <a href="crowdsignin.php">menu</a>
    </div>
    <input type="hidden" id="secure"
        value="<?php echo $sendback["hash"];?>" />
    <input type="hidden" id="skey" value="<?php echo $sendback["salt"];?>" />
    <input type="hidden" id="tcmid" value="<?php echo $tcm_id;?>" />
    <div id="mainpane" class="displayPane">
    </div>
</body>

<?php if($flag==1) echo '<script>$("#mainpane").hide()</script>';?>

<script id="theTmpl" type="text/x-jsrender">
        {{for tasks}}
        <div class="eachtask">
        <div class="eachTaskDesc">{{:desc}}</div>
            <img src="{{>serveImg}}" id="img_{{>tsm_id}}">
            <div class="optionsdiv">
                <div id="show{{>tsm_id}}">
                    {{for responses}}
                        <div class ="chkbox">
                            <label><input name="radiobuttons{{>#parent.parent.data.tsm_id}}" rel="{{>tspd_id}}" type="radio" value="{{>data_entered}}" />{{:data_entered}}</label>
                        </div>
                    {{/for}}
                </div>
                <input type="hidden" value="{{>tsm_id}}" class="keepingIDs" />
                </div>
                <div class="clear"></div>
            </div>
        </div>
        {{/for}}
        <div class="submitpane"><input type="submit" name="submit" id="submitResponses" value="Submit" class="sbmtbtn" /> </div>
</script>
<script type="text/javascript">
var template = $.templates("#theTmpl");

var htmlOutput = template.render(<?php echo json_encode($sendback); ?>);

$("#mainpane").html(htmlOutput);
var availableTags = <?php echo json_encode($sendback['auto']);?>;
var tcj_id = <?php echo $_GET['jid'];?>

$("#submitResponses").click(function(){
    submitdata();
    });

function submitdata()
{
    var sendData = new Array();
    $('input.keepingIDs').each(function( index ) {
        var tsmid = $( this ).val();
        var checkedValue = [];
        var tspdid = 0;
        $('#show'+tsmid+' input:checked').each(function(index){
            checkedValue.push(this.value);
            tspdid = $(this).attr("rel");
        });
        var valInp = checkedValue.join(',');
        sendData.push({"tsmid":tsmid,"value":valInp, "tspdid":tspdid});
        //console.log( index + ": " + $( this ).text() );
    });
    $('#mainpane').html("<img src='images/loader.gif'/>");
    var envelop = {'tcmid':$('#tcmid').val(),'salt':$('#skey').val(),'hash':$('#secure').val(), 'tasks':JSON.stringify(sendData)};
    $.ajax({
        type: "POST",
        url: "votesubmit.php",
        dataType: "json",
        data: envelop
    }).done(function(data) {
        if(data.status==true)
        {
        	location.reload(); 
        }
        else
        {
            window.location.href='index.php';
        }
    });
}
</script>
</html>
