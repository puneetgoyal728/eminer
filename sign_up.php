<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sign Up</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

</head>
<style>
input{
display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
input.invalid, textarea.invalid{
    border: 2px solid red;
}
 
input.valid, textarea.valid{
    border: 2px solid green;
}
#loading{
    width:300px;
    display: none;
   
} 
</style>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Sign Up !!</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" id="login" action="crowdsignupprocess.php" method="post" onsubmit="document.getElementById('signup').style.display='none'; document.getElementById('loading').style.display='block';">
                            <fieldset>
                                <div class="form-group">
                                    <input placeholder="Name" name="name" id="name" type="name" >
                                </div>
                                <div class="form-group">
                                    <input  placeholder="E-mail" name="email" id="login_email" type="email" >
                                </div>
                                <div class="form-group">
                                    <input placeholder="Password" name="password" id="login_password" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" value="Signup" class="submit btn btn-success btn-block btn-lg" id="signup">Signup</button>
                            <img src="images/loading.gif" id="loading" height="42" width="420">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
var flag_email=0;
var flag_pass=0;
var flag_name=0;
$('#name').on('input', function() {
    var input=$(this);
    var is_name=input.val();
    var regex=/^([-a-z0-9_-])+$/i;
    var is_str=regex.test(is_name);
    if(is_name.length>=2 && is_str == true){
        flag_name=1;
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        input.removeClass("invalid").addClass("valid");
        }
    else{
        flag_name=0;
        input.removeClass("valid").addClass("invalid");}
    if(flag_email==0 || flag_pass==0 ||flag_name==0)
    {
        $('.submit').attr("disabled", true);
        $('.submit').css("opacity", 0.30);
    }
    else{
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        }
});

$('#login_password').on('input', function() {
    var input=$(this);
    var is_name=input.val();
    if(is_name.length>=6){
        flag_pass=1;
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        input.removeClass("invalid").addClass("valid");
        }
    else{
        flag_pass=0;
        input.removeClass("valid").addClass("invalid");}
    if(flag_email==0 || flag_pass==0 || flag_name==0)
    {
        $('.submit').attr("disabled", true);
        $('.submit').css("opacity", 0.30);
    }
    else{
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        }
});

$('#login_email').on('input', function() {

    var input=$(this);
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var is_email=re.test(input.val());
    var emailid=$('#login_email').val();
    var type="crowd";
    if(is_email)
    {
        
    $.ajax({
        type: "POST",
        url: "validateemail.php",
        data: { 'email': emailid,'type' : type}
      }).done(function(data) {
            
            if(data=="0")
            {
                flag_email=1;
            input.removeClass("invalid").addClass("valid");
            }
            else
                {
                flag_email=0;
                input.removeClass("valid").addClass("invalid");
                }
            });
    
    }
else{
    flag_email=0;
    input.removeClass("valid").addClass("invalid");}
    if(flag_email==0 || flag_pass==0 || flag_name==0)
    {
        $('.submit').attr("disabled", true);
        $('.submit').css("opacity", 0.30);
    }
    else{
        $('.submit').attr("disabled", false);
        $('.submit').css("opacity", 1);
        }
});


</script>
</html>
