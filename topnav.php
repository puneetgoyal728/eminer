<?php
$tcm_id=$_SESSION["tcm_id"];
$query="SELECT currentwallet from tbl_crowd_master where tcm_id =$tcm_id ";
$querydata = $conn->dbh->query($query);
$result = $querydata->fetch(PDO::FETCH_ASSOC);
$wallet = $result['currentwallet'];
$Data = $conn->dbh->query("SELECT * FROM tbl_crowd_notification where tcm_id=$tcm_id order by tcn_id desc");
$DataResult = $Data->fetchAll(PDO::FETCH_ASSOC);
?>
<div><!-- DIV for top navigation bar -->

            <div class="navbar-header">
                <a class="navbar-brand" href="eminer_home.php">Eminer</a>
            </div>

            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo $wallet."   ";?><i class="fa fa-money fa-fw"></i>
                    </a>
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts" id="notification">
                        <li>
                            <a class="text-center" href="useraccounts.php?f=1">
                                <strong >See All Alerts</strong> <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="useraccounts.php"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        
                        <li class="divider"></li>
                        <li><a href="crowdlogout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            </div><!-- DIV for top navigation bar -->
            <script src="js/jquery-1.11.1.min.js"></script>
<script>
$(document).ready(function(){
    var notify= <?php echo json_encode($DataResult);?>;
    for (var i = 0; i <=  notify.length -1; ++i) {
        var note = "<li><a href='#'><div><i class='fa fa-comment fa-fw'></i>"+notify[i].notification+"<span class='pull-right text-muted small'>"+notify[i].rec_add_date+"</span></div></a></li><li class='divider'></li>";
        $("#notification").append(note);            
    };
});

</script>