<?php
$fieldtype = array("text"=>"Text","num"=>"Numeric","multiple"=>"Multiple Correct","pic"=>"Picture");
$subtype=array("small"=>"Small","large"=>"Large","selone"=>"Select One","selmany"=>"Select Many");
$fieldSubtypeMap = array("text"=>array("small","large"),"multiple"=>array("selone","selmany"));

//this array is only for s1
$maptojobmap = array("text"=>array("small"=>1, "large"=>1),"multiple"=>array("selone"=>2,"selmany"=>3));
//C vishesh karya
//B double entry spelling correct
//A single entry majority vote
$stateType = array("num"=>"A","multiple"=>"A","text"=>"B");
$stageArr = array("s1"=>"Data Entry","s2"=>"Majority Vote","s3"=>"Spell Correct","t"=>"Terminal");
$stageStateInSteps = array("A"=>array("s1","s2","s2","t"),"B"=>array("s1","s1","s3","t"));
?>