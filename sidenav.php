<!-- DIV for side bar begins-->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="eminer_home.php"><i class="fa fa-laptop fa-fw"></i> Home</a>
                        </li>
                         <li>
                            <a href="useraccounts.php"><i class="fa fa-user fa-fw"></i>My Account</a>
                        </li>
                         <li>
                            <a href="changepassword.php"><i class="fa fa-gear fa-fw"></i> Change Password</a>
                        </li>
                         <li>
                            <a href="crowdlogout.php"><i class="fa fa-power-off fa-fw"></i> Log Out</a>
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div> <!-- DIV for side bar ends-->
