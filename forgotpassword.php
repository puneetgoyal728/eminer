<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Forgot password</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">

</head>
<style>
input{
display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
input.invalid, textarea.invalid{
    border: 2px solid red;
}
 
input.valid, textarea.valid{
    border: 2px solid green;
}
#loading{
    width:300px;
    display: none;
   
} 
</style>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Retrieve password !!</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="#" >
                            <fieldset>
                                <div class="form-group" > <span id="note"></span></div>
                                <div class="form-group">
                                    <input  placeholder="E-mail" name="email" id="email" type="email" >
                                </div>
                                
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" value="Signup" class="submit btn btn-success btn-block btn-lg" id="signup">Confirm</button>
                            <img src="images/loading.gif" id="loading" height="42" width="420">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$('#signup').on('click', function(e) {
  e.stopPropagation();
   e.preventDefault();
    var email=$('#email').val();
    
        
    $.ajax({
        type: "POST",
        url: "forgotpasswordprocess.php",
        data: { 'emailid': email}
      }).done(function(data) {
            
            if(data!=""){
            	$('#email').hide(); $('#signup').hide();
    	  $('#note').text("Verification email has been send to "+data+".");
            }
            else
            	 $('#note').text("Enter valid email");
            });
    
    });



</script>
</html>
